package ar.edu.ciu.objetos2.decorator.main;

import ar.edu.ciu.objetos2.decorator.model.Airbag;
import ar.edu.ciu.objetos2.decorator.model.AireAcondicionado;
import ar.edu.ciu.objetos2.decorator.model.SensorEstacionamiento;
import ar.edu.ciu.objetos2.decorator.model.Vendible;
import ar.edu.ciu.objetos2.decorator.model.auto.FiatUno;
import ar.edu.ciu.objetos2.decorator.model.auto.FordFiesta;

public class Main {

  public static void main(String[] args){
    Vendible fiat = new FiatUno("Gris",5);
    System.out.println(fiat.getDescripcion());
    System.out.println("Su precio es: " + fiat.getPrecio());

    //le agrego accesorios al auto base
    System.out.print("Le agrego accesorios al auto");
    fiat = new Airbag(fiat);
    fiat = new AireAcondicionado(fiat);

    System.out.println(fiat.getDescripcion());
    System.out.println("Su precio es: " + fiat.getPrecio());


    Vendible ford = new FordFiesta("Rojo",3);
    System.out.println(ford.getDescripcion());
    System.out.println("Su precio es: " + ford.getPrecio());

    //le agrego accesorios al auto
    System.out.print("Le agrego accesorios al auto");
    ford = new Airbag(ford);
    ford = new AireAcondicionado(ford);
    ford = new SensorEstacionamiento(ford);
    System.out.println(ford.getDescripcion());
    System.out.println("Su precio es: " + ford.getPrecio());

  }
}
