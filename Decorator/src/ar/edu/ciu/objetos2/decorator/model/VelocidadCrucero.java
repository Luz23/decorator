package ar.edu.ciu.objetos2.decorator.model;

public class VelocidadCrucero extends AutoDecorator{

  public VelocidadCrucero(Vendible vendible) {
    super(vendible);
  }

  @Override
  public String getDescripcion() {
    return getVendible().getDescripcion() + " + velocidad crucero";
  }

  @Override
  public int getPrecio() {
    return getVendible().getPrecio() + 18000;
  }
}
