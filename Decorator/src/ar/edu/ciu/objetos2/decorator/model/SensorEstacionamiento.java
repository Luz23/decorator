package ar.edu.ciu.objetos2.decorator.model;

public class SensorEstacionamiento extends  AutoDecorator{

  public SensorEstacionamiento(Vendible vendible) {
    super(vendible);
  }

  @Override
  public String getDescripcion() {
    return getVendible().getDescripcion() + " + sensor estacionamiento";
  }

  @Override
  public int getPrecio() {
    return getVendible().getPrecio() + 16000;
  }
}
