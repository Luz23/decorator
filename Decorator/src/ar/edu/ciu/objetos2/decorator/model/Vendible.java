package ar.edu.ciu.objetos2.decorator.model;

public interface Vendible {

  String getDescripcion();
  int getPrecio();
}
