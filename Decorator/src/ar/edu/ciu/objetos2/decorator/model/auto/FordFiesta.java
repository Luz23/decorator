package ar.edu.ciu.objetos2.decorator.model.auto;

import ar.edu.ciu.objetos2.decorator.model.auto.Auto;

public class FordFiesta extends Auto {


  public FordFiesta(String color, Integer cantPuertas) {
    super(color, cantPuertas);
  }

  @Override
  public String getDescripcion() {
    return "Ford fiesta modelo 2015";
  }

  @Override
  public int getPrecio() {
    return 480000;
  }
}
