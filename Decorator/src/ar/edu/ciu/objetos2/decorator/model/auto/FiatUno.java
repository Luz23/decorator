package ar.edu.ciu.objetos2.decorator.model.auto;

import ar.edu.ciu.objetos2.decorator.model.auto.Auto;

public class FiatUno extends Auto {


  public FiatUno(String color, Integer cantPuertas) {
    super(color, cantPuertas);
  }

  @Override
  public String getDescripcion() {
    return "Fiat uno modelo 2006";
  }

  @Override
  public int getPrecio() {
    return 300000;
  }
}
