package ar.edu.ciu.objetos2.decorator.model;

public class Airbag extends  AutoDecorator{


  public Airbag(Vendible vendible) {
    super(vendible);
  }

  @Override
  public String getDescripcion() {
    return getVendible().getDescripcion() + " + airbag";
  }

  @Override
  public int getPrecio() {
    return getVendible().getPrecio() + 15000;
  }
}
