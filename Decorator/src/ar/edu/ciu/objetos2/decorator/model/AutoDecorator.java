package ar.edu.ciu.objetos2.decorator.model;

public abstract class AutoDecorator implements Vendible{

  protected Vendible vendible;

  public AutoDecorator(Vendible vendible) {
    this.vendible = vendible;
  }

  public Vendible getVendible() {
    return vendible;
  }

  public void setVendible(Vendible vendible) {
    this.vendible = vendible;
  }
}
