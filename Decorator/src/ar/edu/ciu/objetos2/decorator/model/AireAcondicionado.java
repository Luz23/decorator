package ar.edu.ciu.objetos2.decorator.model;

public class AireAcondicionado extends  AutoDecorator{

  public AireAcondicionado(Vendible vendible) {
    super(vendible);
  }

  @Override
  public String getDescripcion() {
    return getVendible().getDescripcion() + " + aire acondicionado";
  }

  @Override
  public int getPrecio() {
    return getVendible().getPrecio() + 8000;
  }
}
