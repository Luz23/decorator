package ar.edu.ciu.objetos2.decorator.model.auto;

import ar.edu.ciu.objetos2.decorator.model.Vendible;

public abstract class Auto implements Vendible {

  protected String color;
  protected Integer cantPuertas;

  public Auto(String color, Integer cantPuertas) {
    this.color = color;
    this.cantPuertas = cantPuertas;
  }

  public String getColor() {
    return color;
  }

  public Integer getCantPuertas() {
    return cantPuertas;
  }
}
